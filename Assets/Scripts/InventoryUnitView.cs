﻿using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InventoryUnitView : MonoBehaviour
{
    [SerializeField]
    private Image _unitImg;
    [SerializeField]
    private Text _titleText;
    [SerializeField]
    private Text _descriptionText;

    public void MyInit(Unit unit)
    {
        
        _titleText.text = unit.dataUnit.title.ToString();
        _descriptionText.text = $"Attack: {unit.dataUnit.attack}\nProtection: {unit.dataUnit.protection}\nSpeed: {unit.dataUnit.speed}\nLuck: {unit.dataUnit.protection}";

        StartCoroutine(LoadPicture(unit.dataUnit.imageUrl));
    }

    IEnumerator LoadPicture(string url)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
        Sprite webSprite = Sprite.Create(webTexture, new Rect(0.0f, 0.0f, webTexture.width, webTexture.height), new Vector2(0, 0), 100);

        _unitImg.sprite = webSprite;

    }
}
