﻿using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Assets.Scripts.com;
using System.IO;

public interface IUnitView
{
    void EditImage(Sprite str);
}

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject _errorCoinsBg;

    [SerializeField]
    private Text _coinsText;

    [SerializeField]
    private GameObject _cardUnitShop;
    [SerializeField]
    private GameObject _cardUnitInventory;

    [SerializeField]
    private GameObject _shopContentTransform;
    [SerializeField]
    private GameObject _inventoryContentTransform;

    private List<Unit> _units = new List<Unit>();
    private Inventory _inventory = new Inventory();
    private const string INVENTORY_KEY = "inventoryKey";
    private const string SHOP_KEY = "shopKey";

    private void Awake()
    {
        EventManager._PURCHASE += Purchase;
    }
    void Start()
    {
        InitializationUnitsShop();
        InitializerUnitsInventory();

        _coinsText.text = _inventory.coins.ToString();

    }

    void Purchase(IData data)
    {

        Unit boughtUnit = (Unit)((EventData)data).data;

        _coinsText.text = _inventory.coins.ToString();
        SaveInventory();
        InitializerUnitInventory(boughtUnit);

    }
    private void LoadInventory()
    {
        //TextAsset ta = Resources.Load<TextAsset>("Test");
        //_inventory = JsonUtility.FromJson<Inventory>(ta.text);

        if (PlayerPrefs.HasKey(INVENTORY_KEY))
        {
            string json = PlayerPrefs.GetString(INVENTORY_KEY);
            _inventory = JsonUtility.FromJson<Inventory>(json);
        }
        else
        {
            TextAsset ta = Resources.Load<TextAsset>("StartCoins");
            _inventory = JsonUtility.FromJson<Inventory>(ta.text);
            SaveInventory();
        }
    }
    private void SaveInventory()
    {
        //string path = Path.Combine(Application.dataPath, "Resources/Test.json");
        //File.WriteAllText(path,JsonUtility.ToJson(_inventory,true));
        string json = JsonUtility.ToJson(_inventory);
        PlayerPrefs.SetString(INVENTORY_KEY, json);
    }

    private void InitializerUnitInventory(Unit unit)
    {
        GameObject view = Instantiate(_cardUnitInventory, _inventoryContentTransform.transform);
        view.GetComponent<InventoryUnitView>().MyInit(unit);
    }
    private void InitializerUnitsInventory()
    {
        foreach (var item in _inventory.units)
        {
            InitializerUnitInventory(item);
        }
    }
    private void InitializationUnitsShop()
    {
        LoadInventory();
        UnitsCreator();

        foreach (var item in _units)
        {
            GameObject view = Instantiate(_cardUnitShop, _shopContentTransform.transform);
            view.GetComponent<ShopUnitView>().MyInit(item);
            view.GetComponent<ShopUnitLogics>().UnitInit(item, _inventory);
        }
    }
    private void UnitsCreator()
    {
        _units.Add(new ShooterUnit());
        _units.Add(new JeepUnit());
        _units.Add(new TankUnit());
        _units.Add(new AirplaneUnit());
    }

}
