﻿using Assets.Scripts.com;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopUnitLogics : MonoBehaviour
{
    [SerializeField]
    private Button _buyButton;
    [SerializeField]
    private Text _quantityText;

    private Unit _unitObj;

    private Inventory _inventory;

    void Start()
    {
        _buyButton.onClick.AddListener(BuyButtonClick);
    }
    public void UnitInit(Unit unit, Inventory inv)
    {
        _inventory = inv;
        _unitObj = unit;
    }
    private void BuyButtonClick()
    {
        ;
        if (_inventory.Purchase(_unitObj) && _unitObj.dataUnit.quantity > 0)
        {
            _inventory.units.Add(_unitObj);
            _unitObj.dataUnit.quantity--;
            _quantityText.text = _unitObj.dataUnit.quantity.ToString();
            EventManager._PURCHASE(new EventData(_unitObj));
        }
        else
        {
            Debug.Log("Huevo");
        }
    }

}
